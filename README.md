# ics-ans-role-mouse-locater

Ansible role to install mouse-locater.

This role is based on a Github project by Martin Tournoj
https://github.com/arp242/find-cursor

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-mouse-locater
```

## License

BSD 2-clause
