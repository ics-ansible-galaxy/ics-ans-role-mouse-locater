import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_find_cursor_app(host):
    cursor_app = host.file("/usr/local/bin/find-cursor")

    assert cursor_app.user == 'root'
    assert cursor_app.group == 'root'
    assert cursor_app.mode == 0o755
